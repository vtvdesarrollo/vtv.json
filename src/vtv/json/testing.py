from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

from plone.testing import z2

from zope.configuration import xmlconfig


class VtvjsonLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import vtv.json
        xmlconfig.file(
            'configure.zcml',
            vtv.json,
            context=configurationContext
        )

        # Install products that use an old-style initialize() function
        #z2.installProduct(app, 'Products.PloneFormGen')

#    def tearDownZope(self, app):
#        # Uninstall products installed above
#        z2.uninstallProduct(app, 'Products.PloneFormGen')

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'vtv.json:default')

VTV_JSON_FIXTURE = VtvjsonLayer()
VTV_JSON_INTEGRATION_TESTING = IntegrationTesting(
    bases=(VTV_JSON_FIXTURE,),
    name="VtvjsonLayer:Integration"
)
VTV_JSON_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(VTV_JSON_FIXTURE, z2.ZSERVER_FIXTURE),
    name="VtvjsonLayer:Functional"
)
