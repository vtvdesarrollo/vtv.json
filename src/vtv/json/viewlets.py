"""
    EJEMPLO DE Viewlet.
"""

# Zope imports
from Acquisition import aq_inner
from zope.interface import Interface
from five import grok
from zope.component import getMultiAdapter

# Plone imports
from plone.app.layout.viewlets.interfaces import *

# The viewlets in this file are rendered on every content item type
grok.context(Interface)

# Use templates directory to search for templates.
grok.templatedir('templates')


class JSViewlets (grok.Viewlet):
    """viewlet que retorna javascript """
    grok.viewletmanager(IPortalFooter)

    def render(self):
        return """
<!--
<script>
//aca va el js
$(document).ready(function(){
	alert('hola mundo');
})
</script>
-->
"""


