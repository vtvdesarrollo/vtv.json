""" Viewlets related to application logic.
"""
# Zope imports
from zope.interface import Interface
from five import grok
from Products.ATContentTypes.interfaces.document import IATDocument
import json

# Search for templates in the 'templates' directory
grok.templatedir('templates')

class JsonDocument(grok.View):
     """ 
     """
     # Vista solo para IATDocument
     grok.context(Interface)

     def render(self):
         return json.dumps({'id': self.context.id,
                           'title': self.context.Title()})
